# Joshua's Dotfiles

Configs for:
- BetterTouchTool
- Brew
- Docker
- Git
- IntelliJ Idea
- iTerm
- Node
- Scripts
- Vim
- Zsh
