javascript: (() => {
    const seconds = Number(prompt("Pause in # seconds?", "5"));
    if (isNaN(seconds)) {
        alert(`${seconds} is not a number!`);
    } else {
        setTimeout(() => {
            debugger;
        }, seconds * 1000);
    }
})();
