javascript: (function() {
    function copyToClipboard(text) {
        if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed"; /* Prevent scrolling to bottom of page in MS Edge.*/
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy"); /* Security exception may be thrown by some browsers.*/
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }
    const branchName = document.getElementsByClassName('branch-name')[0].innerHTML;
    const fragmentsUrl = `https://jira-frontend-branch.staging.atl-paas.net/${branchName.replace('issue/','issue-')}/fragments`;
    console.log(`Fragments URL copied to clipboard: ${fragmentsUrl}`);
    copyToClipboard(fragmentsUrl);
})();
