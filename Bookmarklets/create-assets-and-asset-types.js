javascript: (function() {
  const blessedAddons = [
    'com.oomnitza.assets.jira.plugin',
    'com.device42',
    'popcorn-addon',
    'com.atlassian.servicedesk.connect.test-bed',
    'com.riadalabs.jira.plugins.insight',
    'snipeit-jira-integration',
    'com.reftab.www.jira.plugin',
    'com.ezofficeinventory.asset',
  ];

  const appKey = prompt(`What appKey would you like to use?\n\n- ${blessedAddons.join('\n- ')}]`);

  const assetTypes = ['phone', 'laptop', 'tablet', 'monitor', 'accessories'];
  const assets = {
    phone: ['Apple iPhone X', 'Apple iPhone XS', 'Apple iPhone XR', 'Samsung Galaxy S10', 'Samsung Note 10'],
    laptop: ['Apple MacBook Pro', 'Apple MacBook Air', 'Apple MacBook', 'Dell XPS 13', 'ALIENWARE m17'],
    tablet: ['Apple iPad Air', 'Apple iPad Pro', 'Apple iPad Mini', 'Microsoft Surface Go', 'Samsung Galaxy Tab S4'],
    monitor: ['BenQ PD3200U 32-Inch', 'Asus ROG Swift PG27UQ 27-Inch', 'Dell UltraSharp UP3218K 32-Inch', 'Apple Pro Display XDR 32-Inch', 'LG UltraFine 4K 24-Inch'],
    accessories: ['Apple Wireless Keyboard', 'Apple Magic Mouse 2', 'Apple AirPods', 'Twelve South Curve Stand for MacBook', 'Beats Solo3 Wireless Headphones'],
  };

  const createAssetTypes = () => {
    const promises = [];
    assetTypes
      .map((assetType, index) => ({
        origin: {
          appKey,
          originId: `TYPE-${index + 1}`
        },
        label: {
          value: assetType,
        }
      }))
      .forEach(assetType => {
        promises.push(fetch(`${window.location.origin}/rest/assetapi/asset/type`, {
            method: "PUT",
            headers: {
              "content-type": "application/json",
              accept: "application/json",
            },
            body: JSON.stringify(assetType),
          })
          .catch(err => {
            console.log(err);
          }));
      });
    return Promise.all(promises);
  };

  const createAssets = () => {
    const promises = [];
    assetTypes
      .forEach((assetType, assetTypeIndex) => {
        assets[assetType]
          .map((asset, assetIndex) => ({
            origin: {
              appKey,
              originId: `ASSET-${(assetIndex + 1) + (assetTypeIndex * assetTypes.length)}`
            },
            label: {
              value: asset,
            },
            type: {
              appKey,
              originId: `TYPE-${assetTypeIndex + 1}`
            }
          }))
          .forEach(asset => {
            promises.push(fetch(`${window.location.origin}/rest/assetapi/asset`, {
                method: "PUT",
                headers: {
                  "content-type": "application/json",
                  accept: "application/json",
                },
                body: JSON.stringify(asset),
              })
              .catch(err => {
                console.log(err);
              }));
          });
      });
    return Promise.all(promises);
  };

  const createAssetTypesAndAssets = () => {
    createAssetTypes()
      .then(() => {
        createAssets()
          .then(alert('Successfully created 5 asset types and 20 assets!'))
          .catch(error => alert(`An error occured while creating assets: ${error}`))
      })
      .catch(error => alert(`An error occured while creating asset types: ${error}`));
  };

  createAssetTypesAndAssets();
})();
