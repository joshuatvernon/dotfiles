javascript: (() => {
    var payload = prompt('Set supported products:\n* NEXT_GEN_ONLY\n* NEXT_GEN_AND_CLASSIC\n* CLASSIC_ONLY', 'NEXT_GEN_ONLY');
    fetch("/rest/internal/simplified/1.0/supported-project-types/service_desk", {
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        method: 'PUT',
        body: payload
    }).then((response) => {
        if (response.ok) {
            alert(`Set ${window.location.hostname} to ${payload}! 🎉`);
        } else {
            throw new Error(`Failed to set ${window.location.hostname} to ${payload}! 🔥`);
        }
    }).catch(() => alert(`Failed to set ${window.location.hostname} to ${payload}! 🔥`));
})();
