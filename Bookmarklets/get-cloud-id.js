javascript: (function() {
    function copyToClipboard(text) {
        if (window.clipboardData && window.clipboardData.setData) {
            /*IE specific code path to prevent textarea being shown while dialog is visible.*/
            return clipboardData.setData("Text", text);
        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed"; /* Prevent scrolling to bottom of page in MS Edge.*/
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy"); /* Security exception may be thrown by some browsers.*/
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    }
    const idTag = document.querySelector('meta[name="tenant-id"]') || document.querySelector('meta[name="ajs-cloud-id"]') || (globalAnalyticProperties && globalAnalyticProperties.cloudId);
    if (idTag) {
        const cloudId = idTag.content || idTag;
        copyToClipboard(cloudId);
        console.log(`Copied CloudID to the clipboard: ${cloudId}`);
    } else {
        alert('Couldn\'t find CloudID!');
    }
})();
