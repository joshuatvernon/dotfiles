javascript: (function() {
    const copyToClipboard = (text) => {
        if (window.clipboardData && window.clipboardData.setData) {
            /*IE specific code path to prevent textarea being shown while dialog is visible.*/
            return clipboardData.setData("Text", text);
        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed"; /* Prevent scrolling to bottom of page in MS Edge.*/
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy"); /* Security exception may be thrown by some browsers.*/
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
            }
        }
    };
    const idTag = document.querySelector('meta[name="tenant-id"]') || document.querySelector('meta[name="ajs-cloud-id"]') || (globalAnalyticProperties && globalAnalyticProperties.cloudId);
    let cloudId = 'NO-CLOUD-ID-FOUND';
    if (idTag) {
        cloudId = idTag.content || idTag;
        copyToClipboard(cloudId);
        console.log(`Copied CloudID to the clipboard: ${cloudId}`);
    } else {
        console.log('Couldn\'t find CloudID!');
    }
    const overlay = document.createElement('div');
    const overlayStyle = ` z-index: 1000; position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: rgba(0, 0, 0, 0.5); `;
    overlay.setAttribute('style', overlayStyle);
    const modal = document.createElement('div');
    const modalStyle = ` z-index: 10001; background: white; padding: 20px; border: 1px solid rgba(0, 0, 0, 0.3); box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3); border-radius: 8px; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); `;
    modal.setAttribute('style', modalStyle);
    const title = document.createElement('h3');
    const titleStyle = ` color: DimGrey !important; `;
    title.setAttribute('style', titleStyle);
    title.appendChild(document.createTextNode('Feature flags'));
    title.appendChild(document.createElement('br'));
    title.appendChild(document.createElement('br'));
    modal.appendChild(title);
    const flagInputFieldLabel = document.createElement('h2');
    flagInputFieldLabel.appendChild(document.createTextNode('Feature flag key'));
    const flagInputFieldStyle = ` padding: 5px; border: 2px solid #ccc; border-radius: 4px; outline: 0; `;
    const flagInputFieldHoverStyle = ` padding: 5px; border: 2px solid DimGrey; border-radius: 4px; outline: 0; `;
    const flagInputField = document.createElement('input');
    flagInputField.setAttribute('autofocus', true);
    flagInputField.setAttribute('style', flagInputFieldStyle);
    flagInputField.onfocus = () => {
        flagInputField.setAttribute('style', flagInputFieldHoverStyle);
    };
    flagInputField.onfocusout = () => {
        flagInputField.setAttribute('style', flagInputFieldStyle);
    };
    modal.appendChild(flagInputField);
    const submitButton = document.createElement('button');
    const submitButtonStyle = ` color: #fff !important; background: rgba(127, 255, 0, 0.7); padding: 10px; margin-right: 10px; border-radius: 4px; display: inline-block; border: none; transition: all 0.4s ease 0s; `;
    const submitButtonHoverStyle = ` color: #fff !important; background: rgba(127, 255, 0, 1); padding: 10px; margin-right: 10px; border-radius: 4px; display: inline-block; border: none; box-shadow: 5px 40px -10px rgba(0,0,0,0.57); transition: all 0.4s ease 0s; `;
    const submitButtonDisabledStyle = ` color: #fff !important; background: Grey; padding: 10px; margin-right: 10px; border-radius: 4px; display: inline-block; border: none; transition: all 0.4s ease 0s; `;
    submitButton.setAttribute('style', submitButtonStyle);
    submitButton.onmouseover = () => {
        submitButton.setAttribute('style', submitButtonHoverStyle);
    };
    submitButton.onmouseout = () => {
        submitButton.setAttribute('style', submitButtonStyle);
    };
    submitButton.appendChild(document.createTextNode('Search'));
    const flagNodeList = document.createElement('ul');
    const flagNodeListStyle = ` padding: 0; list-style-type: none; `;
    flagNodeList.setAttribute('style', flagNodeListStyle);
    const flagNodeEmptyState = document.createElement('div');
    flagNodeEmptyState.appendChild(document.createTextNode('No matching feature flags!'));
    flagNodeEmptyState.appendChild(document.createElement('br'));
    flagNodeEmptyState.appendChild(document.createElement('br'));
    const searchFeatureFlags = () => {
        submitButton.disabled = true;
        submitButton.setAttribute('style', submitButtonDisabledStyle);
        if (modal.contains(flagNodeList)) {
            modal.removeChild(flagNodeList);
        }
        if (modal.contains(flagNodeEmptyState)) {
            modal.removeChild(flagNodeEmptyState);
        }
        while (flagNodeList.firstChild) {
            flagNodeList.removeChild(flagNodeList.firstChild);
        }
        const flagInputFieldValue = flagInputField.value;
        let flags = [];
        if (flagInputFieldValue !== '') {
            flags = Object.entries(JSON.parse(document.querySelector('meta[name=ajs-fe-feature-flags]').content)).filter(ff => ff[0].includes(flagInputFieldValue));
        }
        if (flags.length >= 1) {
            for (let i = 0; i < (flags.length > 10 ? 10 : flags.length); i++) {
                const key = flags[i][0];
                const flagNodeKeyWithLink = document.createElement('a');
                flagNodeKeyWithLink.setAttribute('target', '_blank');
                flagNodeKeyWithLink.appendChild(document.createTextNode(key));
                const environment = window.location.href.includes('.jira-dev.') ? 'staging' : 'production';
                flagNodeKeyWithLink.href = `https://app.launchdarkly.com/jira/${environment}/features/${key}/targeting`;
                const status = flags[i][1].value;
                const flagNodeStatus = document.createTextNode(`: ${status}`);
                const flagNode = document.createElement('li');
                flagNode.appendChild(flagNodeKeyWithLink);
                flagNode.appendChild(flagNodeStatus);
                flagNodeList.appendChild(flagNode);
            }
            if (flags.length > 10) {
                flagNodeList.appendChild(document.createTextNode(`+ ${flags.length - 10} others`));
                flagNodeList.appendChild(document.createElement('br'));
            }
            flagNodeList.appendChild(document.createElement('br'));
            modal.insertBefore(flagNodeList, title.nextSibling);
        } else {
            modal.insertBefore(flagNodeEmptyState, title.nextSibling);
        }
        submitButton.disabled = false;
        submitButton.setAttribute('style', submitButtonStyle);
    };
    submitButton.setAttribute('onclick', 'searchFeatureFlags()');
    submitButton.onclick = () => {
        searchFeatureFlags();
    };
    modal.appendChild(document.createElement('br'));
    modal.appendChild(document.createElement('br'));
    modal.appendChild(submitButton);
    const stopClickPropogation = e => {
        e.stopPropagation();
    };
    overlay.setAttribute('onclick', 'stopClickPropogation()');
    modal.onclick = e => {
        stopClickPropogation(e);
    };
    const closeModal = () => {
        document.body.removeChild(overlay);
    };
    overlay.setAttribute('onclick', 'closeModal()');
    overlay.onclick = () => {
        closeModal();
    };
    flagInputField.addEventListener("keyup", e => {
        if (e.keyCode === 13) {
            e.preventDefault();
            submitButton.click();
        }
    });
    flagInputField.focus();
    overlay.appendChild(modal);
    document.body.appendChild(overlay);
}());
